/* eslint-env mocha */
const assert = require('assert')

describe('Index', function () {
  describe('#handle()', function () {
    it('should return Hello World as JSON.', function (done) {
      assert.strictEqual([1, 2, 3].indexOf(4), -1)
      return done()
    })
  })
})
